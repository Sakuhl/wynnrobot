table! {
    federationfeed_listeners (id) {
        id -> Int8,
    }
}

table! {
    livefeedlisteners (id) {
        id -> Int8,
    }
}

table! {
    prefixes (id) {
        id -> Int8,
        prefix -> Varchar,
    }
}

table! {
    warfeedlisteners (id) {
        id -> Int8,
    }
}

allow_tables_to_appear_in_same_query!(
    federationfeed_listeners,
    livefeedlisteners,
    prefixes,
    warfeedlisteners,
);
