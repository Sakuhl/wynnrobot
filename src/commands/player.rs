use ::wynncraft;

use ::serenity::prelude::*;
use serenity::model::prelude::*;

use failure::Error;

use chrono::{Datelike, NaiveDateTime};

pub fn wc_player(ctx: &Context, command: &str, msg: &Message) -> Result<(), Error> {
	if command.len() <= 7 {
		bail!("no player specified");
	}
	let player = &command[7..];

	let player = wynncraft::player(player)?;

	let current_server_msg = if let Some(ref x) = player.meta.location.server {
		format!("\n**Currently online on**: {}", x)
	} else {
		String::new()
	};

	msg.channel_id.say(&ctx.http,
		format!(
"**Player name**: {}
**Guild**: {} (joined {})
**Playtime**: {} hours
**Kills**: {} mobs & {} players
**Chests looted**: {}
**Total level**: {}{}",
			player.username,
			player.guild.name.as_ref().unwrap_or(&"None".to_owned()),
			if player.guild.name != None {
				let date = wynncraft::guild(&player.guild.name.clone().unwrap())?.unwrap().members.iter().filter(|x| x.name == player.username).next().unwrap().joined.clone();
				date[0..date.len()-2].parse::<NaiveDateTime>().map(|x| format!("{} {}, {}", match x.month() {
					1 => "January",
					2 => "February",
					3 => "March",
					4 => "April",
					5 => "Mai",
					6 => "Juni",
					7 => "Juli",
					8 => "August",
					9 => "September",
					10 => "October",
					11 => "November",
					12 => "December",
					_ => "<insert Month here>"
				}, x.day(), x.year())).unwrap_or(date)
			} else { "never".to_owned() },
			player.meta.playtime / 60,
			player.global.mobsKilled, player.global.pvp.kills,
			player.global.chestsFound,
			player.classes.iter().map(|x| x.level).sum::<u64>(),
			current_server_msg
		)
	)?;

	Ok(())
}