use ::wynncraft;

use ::serenity::prelude::*;
use ::serenity::model::prelude::*;

use ::diesel;
use ::diesel::prelude::*;
use ::diesel::result::DatabaseErrorKind;

use ::serde_json;
use ::serde_json::Value;

use ::reqwest::blocking::get;

use ::std::thread;

use failure;
use failure::Error;

use super::establish_connection;

pub fn territory_livefeed(ctx: Context) {
	use models::LivefeedListener;
	use schema::livefeedlisteners::dsl::*;

	let connection = establish_connection();

	let resp = get("https://api.wynncraft.com/public_api.php?action=territoryList").unwrap();

	let mut territories: Value = serde_json::from_reader(resp).unwrap();

	let mut timestamp = territories.as_object().unwrap().get("request").unwrap().as_object().unwrap().get("timestamp").unwrap().as_u64().unwrap();

	loop {
		thread::sleep_ms(20_000); // 20 seconds
		let resp = get("https://api.wynncraft.com/public_api.php?action=territoryList").unwrap();

		let new_territories: Value = serde_json::from_reader(resp).unwrap();

		let new_timestamp = new_territories.as_object().unwrap().get("request").unwrap().as_object().unwrap().get("timestamp").unwrap().as_u64().unwrap();
		if !(new_timestamp > timestamp) { // time went backwards for wynncraft?
			continue;
		}
		timestamp = new_timestamp;

		for key in territories.get("territories").unwrap().as_object().unwrap().keys() {
			let value = territories.get("territories").unwrap().as_object().unwrap().get(key).unwrap();
			let new_value = new_territories.get("territories").unwrap().as_object().unwrap().get(key).unwrap();
			if value.get("guild").unwrap().as_str().unwrap() != new_value.get("guild").unwrap().as_str().unwrap() {
				for listener in livefeedlisteners
					.load::<LivefeedListener>(&connection)
			        .expect("Error loading listeners") {
					let _ = ChannelId(listener.id as u64).say(&ctx.http, format!("{}: ~~{}~~ -> **{}**",
						value.get("territory").unwrap().as_str().unwrap(),
						value.get("guild").unwrap().as_str().unwrap(),
						new_value.get("guild").unwrap().as_str().unwrap()
					));
				}
			}
		}

		territories = new_territories;
	}
}

pub fn federation_livefeed(ctx: Context) {
	use models::FederationfeedListener;
	use schema::federationfeed_listeners::dsl::*;

	let ffa = vec!["Qira's Battle Room", "Hive", "Lava Lake", "Temple of Legends", "Bob's Tomb", "Battle Tower", "Detlas", "Cinfras", "Avos Temple",
		"Factory Entrance", "Swamp Dark Forest Transition Mid", "Emerald Trail", "Rodoroc", "Molten Reach", "Molten Heights Portal", "Lava Lake Bridge",
		"Active Volcano", "Volcanic Slope", "Crater Descent", "Wybel Island", "Herb Cave"];

	let federation_allies = vec!["As Darkness Falls",
		"Angels Of Eternal",
		"DogsAmongUs",
		"DeathReapers",
		"Fantasy",
		"Hall of Fame",
		"Holders Of LE",
		"IceBlue Team",
		"Immortalish",
		"Imperial",
		"Kasai Shinrai",
		"Kingdom Foxes",
		"KingdomPhoenixes",
		"LE Flowers",
		"Paladins United",
		"Serpentem Empire",
		"Sins of Seedia",
		"The Hive",
		"The Divine Swords",
		"Titans Valor",
		"Illustratus"];

	let connection = establish_connection();

	let resp = get("https://api.wynncraft.com/public_api.php?action=territoryList").unwrap();

	let mut territories: Value = serde_json::from_reader(resp).unwrap();

	let mut old_owned = vec![];
	let mut old_owned_ffa = vec![];

	let mut timestamp = territories.as_object().unwrap().get("request").unwrap().as_object().unwrap().get("timestamp").unwrap().as_u64().unwrap();

	loop {
		thread::sleep_ms(30_000); // 30 s
		let resp = get("https://api.wynncraft.com/public_api.php?action=territoryList").unwrap();

		let new_territories: Value = serde_json::from_reader(resp).unwrap();

		let new_timestamp = new_territories.as_object().unwrap().get("request").unwrap().as_object().unwrap().get("timestamp").unwrap().as_u64().unwrap();
		if !(new_timestamp > timestamp) { // time went backwards for wynncraft?
			continue;
		}
		timestamp = new_timestamp;

		let mut assigned_owned_by_enemies = vec![];
		let mut ffa_owned_by_enemies = vec![];

		for key in territories.get("territories").unwrap().as_object().unwrap().keys() {
			let new_value = new_territories.get("territories").unwrap().as_object().unwrap().get(key).unwrap();
			let new_guild = new_value.get("guild").unwrap().as_str().unwrap();
			if !federation_allies.contains(&new_guild) {
				if !ffa.contains(&&**key) {
					assigned_owned_by_enemies.push(key.to_owned());
				} else {
					ffa_owned_by_enemies.push(key.to_owned());
				}
			}
		}

		if old_owned != assigned_owned_by_enemies || old_owned_ffa != ffa_owned_by_enemies {
			for listener in federationfeed_listeners
				.load::<FederationfeedListener>(&connection)
		    	.expect("Error loading listeners") {
				let _ = ChannelId(listener.id as u64).say(&ctx.http, format!("Assigned owned by others: {}
FFA owned by others: {}",
					assigned_owned_by_enemies.join(", "),
					ffa_owned_by_enemies.join(", ")
				));
			}
			old_owned = assigned_owned_by_enemies;
			old_owned_ffa = ffa_owned_by_enemies;
		}

		territories = new_territories;
	}
}

pub fn war_livefeed(ctx: Context) {
	use models::WarfeedListener;
	use schema::warfeedlisteners::dsl::*;

	let connection = establish_connection();

	let resp = get("https://api.wynncraft.com/public_api.php?action=onlinePlayers").unwrap();

	let mut players: Value = serde_json::from_reader(resp).unwrap();

	loop {
		thread::sleep_ms(30_000); // 30 seconds
		let resp = get("https://api.wynncraft.com/public_api.php?action=onlinePlayers").unwrap();

		let new_players: Value = serde_json::from_reader(resp).unwrap();

		for key in new_players.as_object().unwrap().keys() {
			if key == "request" {
				continue;
			}
			if !key.starts_with("WAR") {
				continue;
			}
			let war_players = new_players.get(key).unwrap().as_array().unwrap();
			for player in war_players.into_iter() {
				let old_server = players.as_object().unwrap().iter().map(|(server, players)| {
					if server == "request" {
						(&**server, false)
					} else {
						(&**server, players.as_array().unwrap().contains(player))
					}
				}).filter(|&(_, is_in_it)| is_in_it).next().unwrap_or(("WC?", true)).0;
				if old_server != key {
					let player = player.as_str().unwrap();
					let guild = wynncraft::player(player).map(|x| x.guild.name.unwrap_or("none".to_owned())).unwrap_or("?".to_owned());
					for listener in warfeedlisteners
						.load::<WarfeedListener>(&connection)
			        	.expect("Error loading listeners") {
						let _ = ChannelId(listener.id as u64).say(&ctx.http, format!("{} ({}): ~~{}~~ -> **{}**",
							player.replace('_', "\\_"),
							guild,
							old_server,
							key
						));
					}
				}
			}
		}

		players = new_players;
	}
}

pub fn wc_livefeed(ctx: &Context, msg: &Message) -> Result<(), Error> {
	use models::LivefeedListener;
	use schema::livefeedlisteners;

	let connection = establish_connection();

	if let Err(error) = diesel::insert_into(livefeedlisteners::table)
        .values(&LivefeedListener {
			id: msg.channel_id.0 as i64
		})
        .execute(&connection) {
		if let diesel::result::Error::DatabaseError(DatabaseErrorKind::UniqueViolation, _) = error {
			bail!(failure::err_msg("already enabled"));
		} else {
			bail!(error);
		}
	}

	msg.channel_id.say(&ctx.http, "Success!")?;

	Ok(())
}

pub fn wc_unlivefeed(ctx: &Context, msg: &Message) -> Result<(), Error> {
	use schema::livefeedlisteners::dsl::*;

	let connection = establish_connection();

	let channel_id = msg.channel_id.0 as i64;
	diesel::delete(livefeedlisteners.filter(id.eq(channel_id)))
        .execute(&connection)?;

	msg.channel_id.say(&ctx.http, "Success!")?;

	Ok(())
}

pub fn wc_federationfeed(ctx: &Context, msg: &Message) -> Result<(), Error> {
	use models::FederationfeedListener;
	use schema::federationfeed_listeners;

	let connection = establish_connection();

	if let Err(error) = diesel::insert_into(federationfeed_listeners::table)
        .values(&FederationfeedListener {
			id: msg.channel_id.0 as i64
		})
        .execute(&connection) {
		if let diesel::result::Error::DatabaseError(DatabaseErrorKind::UniqueViolation, _) = error {
			bail!(failure::err_msg("already enabled"));
		} else {
			bail!(error);
		}
	}

	msg.channel_id.say(&ctx.http, "Success!")?;

	Ok(())
}

pub fn wc_unfederationfeed(ctx: &Context, msg: &Message) -> Result<(), Error> {
	use schema::federationfeed_listeners::dsl::*;

	let connection = establish_connection();

	let channel_id = msg.channel_id.0 as i64;
	diesel::delete(federationfeed_listeners.filter(id.eq(channel_id)))
        .execute(&connection)?;

	msg.channel_id.say(&ctx.http, "Success!")?;

	Ok(())
}

pub fn wc_warfeed(ctx: &Context, msg: &Message) -> Result<(), Error> {
	use models::WarfeedListener;
	use schema::warfeedlisteners;

	let connection = establish_connection();

	if let Err(error) = diesel::insert_into(warfeedlisteners::table)
        .values(&WarfeedListener {
			id: msg.channel_id.0 as i64
		})
        .execute(&connection) {
		if let diesel::result::Error::DatabaseError(DatabaseErrorKind::UniqueViolation, _) = error {
			bail!(failure::err_msg("already enabled"));
		} else {
			bail!(error);
		}
	}

	msg.channel_id.say(&ctx.http, "Success!")?;

	Ok(())
}

pub fn wc_unwarfeed(ctx: &Context, msg: &Message) -> Result<(), Error> {
	use schema::warfeedlisteners::dsl::*;

	let connection = establish_connection();

	let channel_id = msg.channel_id.0 as i64;
	diesel::delete(warfeedlisteners.filter(id.eq(channel_id)))
        .execute(&connection)?;

	msg.channel_id.say(&ctx.http, "Success!")?;

	Ok(())
}
