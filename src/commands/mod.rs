use ::wynncraft::APIError;

use ::serenity;
use ::serenity::prelude::*;
use ::serenity::model::prelude::*;

use ::reqwest::blocking::get;

use ::serde_json;
use ::serde_json::Value;

use diesel;
use diesel::pg::PgConnection;
use diesel::prelude::*;

use failure;
use failure::Error;

use std::env;

pub mod player;
pub mod guild;
pub mod feed;

pub use player::*;
pub use guild::*;
pub use feed::{wc_livefeed, wc_unlivefeed, wc_warfeed, wc_unwarfeed, wc_federationfeed, wc_unfederationfeed};

pub fn wc_prefix(ctx: &Context, command: &str, msg: &Message) -> Result<(), Error> {
	use models::PrefixConfig;
	use schema::prefixes::dsl::*;

	let connection = establish_connection();

	let new_prefix = &command[7..].to_owned();

	diesel::insert_into(prefixes)
        .values(&PrefixConfig {
			id: msg.guild_id.unwrap().0 as i64,
			prefix: new_prefix.clone()
		})
		.on_conflict(id)
		.do_update()
		.set(prefix.eq(new_prefix))
		.execute(&connection)?;

	msg.channel_id.say(&ctx.http, format!("Prefix is now {:?}", new_prefix))?;

	Ok(())
}

pub fn wc_status(ctx: &Context, msg: &Message) -> Result<(), Error> {
	let resp = get("https://api.wynncraft.com/public_api.php?action=onlinePlayersSum")?.text().unwrap();

	let value: Value = if let Ok(error) = serde_json::from_str::<APIError>(&resp) {
		bail!("API error ({})", error.error);
	} else {
		serde_json::from_str(&resp)?
	};

	msg.channel_id.say(&ctx.http,
		format!("{} players online", value.get("players_online").unwrap().as_u64().unwrap())
	)?;

	Ok(())
}

pub fn wc_territory(ctx: &Context, command: &str, msg: &Message) -> Result<(), Error> {
	if command.len() <= 10 {
		bail!("no territory specified");
	}

	let territory = &command[10..];

	let resp = get("https://api.wynncraft.com/public_api.php?action=territoryList")?.text().unwrap();

	let territories: Value = if let Ok(error) = serde_json::from_str::<APIError>(&resp) {
		bail!("API error ({})", error.error);
	} else {
		serde_json::from_str(&resp)?
	};

	let mut message = String::new();
	for value in territories.get("territories").unwrap().as_object().unwrap().values() {
		if value.get("territory").unwrap().as_str().unwrap().contains(territory) {
			message.push_str(&format!("
**Territory**: {}
**Owner**: {}
**Acquired at**: {}
",
				value.get("territory").unwrap().as_str().unwrap(),
				value.get("guild").unwrap().as_str().unwrap(),
				value.get("acquired").unwrap().as_str().unwrap()
			))
		}
	}

	if !message.is_empty() {
		msg.channel_id.say(&ctx.http, message)?;
	}
	
	Ok(())
}

pub fn wc_info(ctx: &Context, msg: &Message) -> Result<(), Error> {
	let guilds = get_guild_count(ctx)?;
	msg.channel_id.say(&ctx.http, format!("Developer: Wurst#1783
Forum thread: https://forums.wynncraft.com/threads/discord-bot.212142/
Discord: https://discord.gg/GbN7HeG
Version: {}
Servers: {}", env!("CARGO_PKG_VERSION"), guilds))?;

	Ok(())
}

fn get_guild_count(ctx: &Context) -> Result<usize, Error> {
	let mut guilds = 0;
	let mut new_guilds = ctx.http.get_guilds(&serenity::http::GuildPagination::After(GuildId(0)), 100)?;
	while !new_guilds.is_empty() {
		guilds += new_guilds.len();
		new_guilds = ctx.http.get_guilds(&serenity::http::GuildPagination::After(new_guilds.last().unwrap().id), 100)?;
	}
	Ok(guilds)
}

pub fn wc_help(ctx: &Context, msg: &Message) -> Result<(), Error> {
	if let Err(_) = msg.author.dm(ctx, |f| f.content("Command overview: (prefix is wc!)

Get information:
	guild <name/prefix> - show guild stats and territories
	player <name> - show player stats and currently online world
	territory <(partial) name> - show territories
	topguilds <limit> - show top guilds

Livefeeds:
	livefeed, unlivefeed - territory takeover feed
	warfeed, unwarfeed - war server login feed (lists tping servers)

Crop screenshots:
	crop - crops last posted picture
	crop <url> - crops image from the internet

Misc.:
	help - get command help
	info - show bot info
	status - show online players

Visit the Discord for more help: https://discord.gg/GbN7HeG")) {
		bail!("could not send DM");
	} else {
		let _ = msg.channel_id.say(&ctx.http, "Direct message sent sucessfully!");
		Ok(())
	}
}

pub fn wc_crop(ctx: &Context, command: &str, msg: &Message) -> Result<(), Error> {
	let url = &command[5..];
	let client = reqwest::blocking::ClientBuilder::new().timeout(::std::time::Duration::from_secs(10000)).build().unwrap();
	let new_url = client.get(&format!("https://wynncraft-autocrop.herokuapp.com/crop?url={}", url)).send()?.text().unwrap();
	if new_url.starts_with("https://res.cloudinary.com") {
		for chunk in new_url.split(' ').collect::<Vec<_>>().chunks(21) {
			msg.channel_id.say(&ctx.http, &chunk.join(" "))?;
		}
	} else {
		bail!(failure::err_msg("could not crop image"));
	}

	Ok(())
}

pub fn wc_crop_discord_upload(ctx: &Context, msg: &Message) -> Result<(), Error> {
	let last = &msg.channel_id.messages(&ctx.http, |g| g.before(msg.id).limit(1))?[0];
	let mut attachments = last.attachments.clone();
	attachments.extend(msg.attachments.clone());
	for attachment in &attachments {
		let url = &attachment.url;
		let new_url = get(&format!("https://wynncraft-autocrop.herokuapp.com/crop?url={}", url))?.text().unwrap();
		if new_url.starts_with("https://res.cloudinary.com") {
			msg.channel_id.say(&ctx.http, &new_url)?;
		} else {
			bail!(failure::err_msg("could not crop image"));
		}
	}
	
	Ok(())
}

pub fn wc_shout(ctx: &Context, command: &str, msg: &Message) -> Result<(), Error> {
	let shout = &command[6..];
	for guild_id in ctx.http.get_current_user().unwrap().guilds(&ctx.http).unwrap() {
		println!("Showing {:?}", guild_id.name);
		let mut wynnbots = vec![];
		let mut botchannels = vec![];
		let mut bots = vec![];
		let mut general = vec![];
		let mut others = vec![];
		for (_channel_id, channel) in Guild::get(&ctx.http, &guild_id).unwrap().channels(&ctx.http).unwrap() {
			if channel.kind != ChannelType::Text {
				continue;
			}
			if channel.name.contains("wynnbot") {
				//println!("{:?}: {:?}", channel_id, channel);
				wynnbots.push(channel);
				continue;
			}
			if channel.name.contains("bot-channel") || channel.name.contains("bot_channel") {
				//println!("{:?}: {:?}", channel_id, channel);
				botchannels.push(channel);
				continue;
			}
			if channel.name.contains("bot") {
				//println!("{:?}: {:?}", channel_id, channel);
				bots.push(channel);
				continue;
			}
			if channel.name == "general" {
				//println!("{:?}: {:?}", channel_id, channel);
				general.push(channel);
				continue;
			}
			others.push(channel);
		}
		/*
		println!("{:?}\n{:?}\n{:?}\n{:?}\n{:?}",
			wynnbots.into_iter().map(|x| x.name).collect::<Vec<_>>(),
			botchannels.into_iter().map(|x| x.name).collect::<Vec<_>>(),
			bots.into_iter().map(|x| x.name).collect::<Vec<_>>(),
			general.into_iter().map(|x| x.name).collect::<Vec<_>>(),
			others.into_iter().map(|x| x.name).collect::<Vec<_>>()
		);
		*/
		wynnbots.extend(botchannels);
		wynnbots.extend(bots);
		wynnbots.extend(general);
		wynnbots.extend(others);
		for channel in &wynnbots {
			println!("posting in {:?}", channel.name);
			/*
			if guild_id.name != "WynnBot" {
				println!("jk im not");
				break;
			}
			*/
			//println!("doing it");
			if let Ok(_) = channel.say(&ctx.http, shout) {
				println!("success!");
				break;
			}
		}
	}
	msg.channel_id.say(&ctx.http, "Success!")?;
	Ok(())
}

pub fn establish_connection() -> PgConnection {
    let database_url = env::var("DATABASE_URL")
        .expect("DATABASE_URL must be set");
    PgConnection::establish(&database_url)
        .expect(&format!("Error connecting to {}", database_url))
}