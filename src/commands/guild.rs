use ::wynncraft;
use ::wynncraft::APIError;

use ::serenity::prelude::*;
use ::serenity::model::prelude::*;

use ::reqwest::blocking::get;

use ::serde_json;
use ::serde_json::Value;

use failure::Error;

use ::std::collections::HashSet;

use ::ResponseError;

pub fn wc_guild(ctx: &Context, command: &str, msg: &Message) -> Result<(), Error> {
	if command.len() <= 6 {
		bail!("no guild specified");
	}
	let guild_name = &command[6..];
	
	let guild;
	if guild_name.len() <= 3 {
		let guild_ = {::GUILD_PREFIXES.read().get(guild_name).map(|x| Some(x.to_owned()))}.unwrap_or(guild_by_prefix(guild_name)?);
		if let Some(guild_) = guild_ {
			{::GUILD_PREFIXES.write().insert(guild_name.to_owned(), guild_.clone());}
			if let Some(guild_) = wynncraft::guild(&guild_)? {
				guild = guild_;
			} else {
				// this shouldnt happen
				bail!(ResponseError::UnknownGuild { name: guild_.to_owned() });
			}
		} else {
			bail!(ResponseError::UnknownGuildPrefix { name: guild_name.to_owned() });
		}
	} else {
		if let Some(guild_) = wynncraft::guild(guild_name)? {
			guild = guild_;
		} else {
			bail!(ResponseError::UnknownGuild { name: guild_name.to_owned() });
		}
	}

	let message = (|| {
	let resp = get("https://api.wynncraft.com/public_api.php?action=territoryList")?
		.error_for_status()?
		.text()?;

	let territories: Value = if let Ok(error) = serde_json::from_str::<APIError>(&resp) {
		bail!("API error ({})", error.error);
	} else {
		serde_json::from_str(&resp)?
	};

	let mut message = String::new();
	//let mut territories_count = 0;
	for value in territories.get("territories").unwrap().as_object().unwrap().values() {
		if value.get("guild").unwrap().as_str().unwrap() == guild.name {
			message.push_str(&format!("
**Territory**: {}",
				value.get("territory").unwrap().as_str().unwrap()
			));
			//territories_count += 1;
		}
	}
	Ok(message)
	})().unwrap_or_else(|e| format!("\nError getting territories: {}", e));

	let online_players = (|| {
		let resp = get("https://api.wynncraft.com/public_api.php?action=onlinePlayers")?;

		let players: Value = serde_json::from_reader(resp)?;
		let mut online_players = HashSet::new();

		for key in players.as_object().unwrap().keys() {
			if key == "request" {
				continue;
			}
			let players = players.get(key).unwrap().as_array().unwrap();
			for p in players {
				online_players.insert(p.as_str().unwrap().to_owned());
			}
		}
		let guild_online_players = guild.members.iter().filter(|x| online_players.contains(&x.name)).map(|x| &*x.name).collect::<Vec<_>>().as_slice().join(", ");
		Ok(guild_online_players)
	})().unwrap_or_else(|e: Error| format!("\nError getting online players: {}", e));

	let full_msg = format!(
"**Guild**: {}
**prefix**: {}
**Created**: {}
**Level**: {}
**Members**: {}
**Online members**: {}",
			guild.name,
			guild.prefix,
			guild.created_friendly,
			guild.level,
			guild.members.len(),
			online_players
		) + &message;
	for part in full_msg.split('\n').collect::<Vec<_>>().chunks(48) {
		msg.channel_id.say(&ctx.http, part.join("\n"))?;
	}
	
	Ok(())
}

fn guild_by_prefix(prefix: &str) -> Result<Option<String>, Error> {
	let resp = get("https://wynnapi.herokuapp.com/list")?.text().unwrap().replace(",]", "]");

	let guilds: Value = serde_json::from_str(&resp)?;

	for guild in guilds.get("guilds").unwrap().as_array().unwrap() {
		if guild.get("prefix").unwrap().as_str().unwrap() == prefix {
			return Ok(Some(guild.get("name").unwrap().as_str().unwrap().to_owned()));
		}
	}
	for guild in guilds.get("guilds").unwrap().as_array().unwrap().into_iter().rev() {
		if guild.get("prefix").unwrap().as_str().unwrap().to_lowercase() == prefix.to_lowercase() {
			return Ok(Some(guild.get("name").unwrap().as_str().unwrap().to_owned()));
		}
	}
	Ok(None)
}

pub fn wc_topguilds(ctx: &Context, command: &str, msg: &Message) -> Result<(), Error> {
	let limit: usize = command[10..].parse().unwrap();

	wc_topguilds_limit(ctx, msg, limit)
}

pub fn wc_topguilds_limit(ctx: &Context, msg: &Message, limit: usize) -> Result<(), Error> {
	let leaderboard = wynncraft::guild_leaderboard()?;

	let mut text = "```".to_owned();
	for guild in leaderboard.data[..limit].into_iter() {
		text += &format!("#{:02}: {:18} [{:3}] - {:3} territories\n", guild.num, guild.name, guild.prefix, guild.territories)
	}
	text += "```";

	msg.channel_id.say(&ctx.http, text)?;

	Ok(())
}