use super::schema::*;

#[derive(Queryable, Insertable)]
#[table_name="livefeedlisteners"]
pub struct LivefeedListener {
    pub id: i64
}

#[derive(Queryable, Insertable)]
#[table_name="federationfeed_listeners"]
pub struct FederationfeedListener {
    pub id: i64
}

#[derive(Queryable, Insertable)]
#[table_name="warfeedlisteners"]
pub struct WarfeedListener {
    pub id: i64
}

#[derive(Queryable, Insertable)]
#[table_name="prefixes"]
pub struct PrefixConfig {
    pub id: i64,
	pub prefix: String
}