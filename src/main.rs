extern crate wynncraft;

extern crate serenity;
use serenity::prelude::*;
use serenity::model::prelude::{Guild, Channel, Message, GuildChannel, PrivateChannel, User, Ready, Activity};

extern crate reqwest;

extern crate serde_json;

#[macro_use]
extern crate diesel;
use diesel::prelude::*;

#[macro_use]
extern crate failure;
use failure::Error;

#[macro_use]
extern crate failure_derive;

#[macro_use]
extern crate configure;
use configure::Configure;

extern crate serde;
#[macro_use]
extern crate serde_derive;

#[macro_use]
extern crate lazy_static;

extern crate strsim;

extern crate chrono;

use std::collections::HashMap;
use std::env;
use std::panic;
use std::thread;

mod models;
mod schema;
use schema::prefixes;
mod commands;
use commands::*;

lazy_static! {
	static ref GUILD_PREFIXES: RwLock<HashMap<String, String>> = RwLock::new(HashMap::new());
}

#[derive(Deserialize, Configure)]
#[serde(default)]
struct Config {
	prefix: String,
	feeds: bool
}

impl Default for Config {
	fn default() -> Config {
		Config {
			prefix: "wcdev!".to_owned(),
			feeds: false
		}
	}
}

#[derive(Debug, Fail)]
pub enum ResponseError {
	#[fail(display = "unknown command: {}", name)]
	UnknownCommand {
		name: String
	},
	#[fail(display = "unknown guild: {}", name)]
	UnknownGuild {
		name: String
	},
	#[fail(display = "unknown guild prefix: {}", name)]
	UnknownGuildPrefix {
		name: String
	}
}

struct Handler {
	feeds: bool
}

impl EventHandler for Handler {
	// Set a handler for the `on_message` event - so that whenever a new message
	// is received - the closure (or function) passed will be called.
	//
	// Event handlers are dispatched through multi-threading, and so multiple
	// of a single event can be dispatched simultaneously.
	fn message(&self, ctx: Context, msg: Message) {
		if msg.author.name == "WynnBot" {
			return;
		}
		let cfg = Config::generate().unwrap();
		let prefix = cfg.prefix;
		let connection = commands::establish_connection();

		let mut prefix = prefixes::table
			.select(prefixes::prefix)
			.filter(prefixes::id.eq(msg.guild_id.map(|x| x.0).unwrap_or(0) as i64))
			.load::<String>(&connection)
			.expect("Error loading prefix").get(0)
			.map(|x| x.to_owned())
			.unwrap_or(prefix);
		
		if msg.is_private() {
			prefix = String::new();
		}

		if let Err(error) = self.process_message(&ctx, &msg, &prefix) {
			//eprintln!("Error: {}", error);
			let _ = msg.channel_id.say(&ctx.http, format!("Error: {}", error));
			for cause in error.causes().skip(1) {
				//eprintln!("caused by: {}", cause);
				let _ = msg.channel_id.say(&ctx.http, format!("caused by: {}", cause));
			}
		}

		/*
		if msg.content.starts_with(&prefix) || msg.content.contains("<@392763365409292298>") {
			let command = &msg.content;
			// log
			if let Ok(user) = "<@210743594061922306>".parse::<User>() {
				let name = if ctx.cache.read().private_channel(msg.channel_id).is_some() {
					"DM".to_owned()
				} else {
					ctx.cache.read().guild_channel(msg.channel_id).map(|x| x.read().guild_id.to_partial_guild(&ctx.http).unwrap().name).unwrap_or("guild not in cache".to_owned())
				};
				let _ = user.dm(&ctx, |f| f.content(format!("{}: {}: {}: {}", name, msg.channel_id, msg.author, command)));
			}
		}
		*/
	}

	// Set a handler to be called on the `on_ready` event. This is called when a
	// shard is booted, and a READY payload is sent by Discord. This payload
	// contains data like the current user's guild Ids, current user data,
	// private channels, and more.
	//
	// In this case, just print what the current bot's username is.
	fn ready(&self, ctx: Context, ready: Ready) {
		println!("{} is connected!", ready.user.name);
		ctx.set_activity(Activity::playing("wc!help"));
		println!("Currently playing wc!help");
		if self.feeds {
			println!("Starting daemons...");
			start_daemons(&ctx);
		}
	}
}

lazy_static! {
	static ref CS: Mutex<Option<String>> = Mutex::new(None);
}

#[derive(Deserialize)]
struct CBResponse {
	cs: String,
	output: String
}

fn cleverbot(ctx: &Context, msg: &Message) -> Result<String, Error> {
	let _ = msg.channel_id.broadcast_typing(&ctx.http);
	let resp = reqwest::blocking::get(&format!("http://www.cleverbot.com/getreply?key=CC7wgO9gQl7yPjB54eDLHmM5Jmg&input={}&cb_settings_tweak1=0&cb_settings_tweak2=100&cb_settings_tweak3=100{}",
			msg.content.replace("<@392763365409292298>", ""), (*CS).lock().clone().map(|x| format!("&cs={}", x)).unwrap_or_else(String::new)))?
		.error_for_status()?
		.text()?;
	thread::sleep_ms(500);
	let resp: CBResponse = serde_json::from_str(&resp)?;
	msg.channel_id.say(&ctx.http, format!("{} {}", msg.author.mention(), resp.output))?;
	Ok(resp.cs)
}

impl Handler {
	fn process_message(&self, ctx: &Context, msg: &Message, prefix: &str) -> Result<(), Error> {
		if msg.content == "<@392763365409292298> has big gay" {
			let _ = msg.channel_id.broadcast_typing(&ctx.http);
			thread::sleep_ms(5000);
			let _ = msg.channel_id.say(&ctx.http, "<@395463760841539584> has bigger gay");
			return Ok(());
		}
		if msg.content == "<@392763365409292298> has biggest gay" {
			let _ = msg.channel_id.broadcast_typing(&ctx.http);
			thread::sleep_ms(5000);
			let _ = msg.channel_id.say(&ctx.http, "<@395463760841539584> no u");
			return Ok(());
		}
		/*
		if msg.content.contains("<@392763365409292298>") {
			if let Ok(cs) = cleverbot(msg) {
				*(*CS).lock() = Some(cs);
				return Ok(());
			} else {
				*(*CS).lock() = None;
				self.process_message(msg, prefix)?;
				return Ok(());
			}
		}
		*/
		if !msg.content.starts_with(&prefix) {
			return Ok(());
		}
		let mut command = msg.content[prefix.len()..].to_owned();
		if command.starts_with("territories ") {
			command = command.replacen("territories", "guild", 1);
		}
		if command.starts_with("top ") || command == "top" {
			command = command.replacen("top", "topguilds", 1);
		}
		if command.starts_with("leaderboard") {
			command = command.replacen("leaderboard", "topguilds", 1);
		}
		if command.starts_with("stats") {
			command = command.replacen("stats", "guild", 1);
		}
		let command = &command;

		if command.starts_with("guild") {
			wc_guild(ctx, command, msg)?;
		} else if command == "topguilds" {
			wc_topguilds_limit(ctx, msg, 10)?;
		} else if command.starts_with("topguilds ") {
			wc_topguilds(ctx, command, msg)?;
		} else if command.starts_with("territory") {
			wc_territory(ctx, command, msg)?;
		} else if command == "status" {
			wc_status(ctx, msg)?;
		} else if command.starts_with("player") {
			wc_player(ctx, command, msg)?;
		} else if command.starts_with("livefeed") {
			wc_livefeed(ctx, msg)?;
		} else if command.starts_with("unlivefeed") {
			wc_unlivefeed(ctx, msg)?;
		} else if command.starts_with("federationfeed") {
			wc_federationfeed(ctx, msg)?;
		} else if command.starts_with("unfederationfeed") {
			wc_unfederationfeed(ctx, msg)?;
		} else if command == "info" || command == "" {
			wc_info(ctx, msg)?;
		} else if command == "help" {
			wc_help(ctx, msg)?;	
		} else if command.starts_with("crop ") {
			wc_crop(ctx, command, msg)?;
		} else if command == "crop" {
			wc_crop_discord_upload(ctx, msg)?;
		} else if command.starts_with("warfeed") {
			wc_warfeed(ctx, msg)?;
		} else if command.starts_with("unwarfeed") {
			wc_unwarfeed(ctx, msg)?;
		} else if command.starts_with("shout") {
			if msg.author.name == "Wurst" && msg.author.discriminator == 1783 {
				wc_shout(ctx, command, msg)?;
			}
		} else if command.starts_with("prefix") {
			wc_prefix(ctx, command, msg)?;
		} else {
			let commands = ["topguilds", "leaderboard", "top", "prefix", "warfeed", "unwarfeed", "livefeed", "unlivefeed", "status", "crop", "guild", "stats", "territories", "player", "territory", "info", "help"];
			let mut min = usize::max_value();
			let mut min_command = "help";
			for command_b in &commands {
				let distance = strsim::levenshtein(command, command_b);
				if distance <= min {
					min = distance;
					min_command = command_b;
				}
			}
			if min < 25 {
				msg.channel_id.say(&ctx.http, &format!("Unknown command. Did you mean {:?}?", min_command))?;
			} else {
				bail!(ResponseError::UnknownCommand { name: command.to_owned() });
			}
		}
		return Ok(());
	}
}

fn main() {
	use_default_config!();
	let cfg = Config::generate().expect("config error");

	// Configure the client with your Discord bot token in the environment.
	let token = env::var("DISCORD_TOKEN").expect("DISCORD_TOKEN must be set");
	env::var("DATABASE_URL")
		.expect("DATABASE_URL must be set");

	loop {
		// Create a new instance of the Client, logging in as a bot. This will
		// automatically prepend your bot token with "Bot ", which is a requirement
		// by Discord for bot users.
		let mut client = Client::new(&token, Handler { feeds: cfg.feeds }).unwrap();

		// Finally, start a single shard, and start listening to events.
		//
		// Shards will automatically attempt to reconnect, and will perform
		// exponential backoff until it reconnects.
		if let Err(why) = client.start() {
			println!("Client error: {:?}", why);
		}
	}
}

fn start_daemons(ctx1: &Context) {
	let ctx = ctx1.clone();
	let territory_feed = thread::Builder::new()
		.spawn(move || {
			loop {
				let ctx = ctx.clone();
				thread::Builder::new().spawn(|| { commands::feed::territory_livefeed(ctx) }).unwrap().join();
			}
		})
		.unwrap();
	let ctx = ctx1.clone();
	let war_feed = thread::Builder::new()
		.spawn(move || {
			loop {
				let ctx = ctx.clone();
				thread::Builder::new().spawn(|| { commands::feed::war_livefeed(ctx) }).unwrap().join();
			}
		})
		.unwrap();
	let ctx = ctx1.clone();
	let federation_feed = thread::Builder::new()
		.spawn(move || {
			loop {
				let ctx = ctx.clone();
				thread::Builder::new().spawn(|| { commands::feed::federation_livefeed(ctx) }).unwrap().join();
			}
		})
		.unwrap();
	println!("Started daemons.");
}
